module create_positional_test
    use cmdff, only: positional_t, create_new_positional_string_argument
    use erloff, only: ErrorList_t
    use Vegetables_m, only: Result_t, TestItem_t, assertThat, describe, it

    implicit none
    private

    public :: test_creating_positional_argument
contains
    function test_creating_positional_argument() result(tests)
        type(TestItem_t) :: tests

        type(TestItem_t) :: individual_tests(2)

        individual_tests(1) = it( &
                "can be created to capture a string", check_plain_string_argument)
        individual_tests(2) = it( &
                "can be given a description", check_string_argument_with_description)
        tests = describe("a positional argument", individual_tests)
    end function

    function check_plain_string_argument() result(result_)
        type(Result_t) :: result_

        type(positional_t) :: argument
        type(ErrorList_t) :: errors

        call create_new_positional_string_argument( &
                name = "arg1", &
                required = .false., &
                errors = errors, &
                argument = argument)

        result_ = assertThat(.not.errors%hasAny(), errors%toString())
    end function

    function check_string_argument_with_description() result(result_)
        type(Result_t) :: result_

        type(positional_t) :: argument
        type(ErrorList_t) :: errors

        call create_new_positional_string_argument( &
                name = "arg1", &
                description = "this is a string argument", &
                required = .false., &
                errors = errors, &
                argument = argument)

        result_ = assertThat(.not.errors%hasAny(), errors%toString())
    end function
end module
