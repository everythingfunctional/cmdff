program main
    use create_parser_test, only: &
            create_parser_creating_parser => test_creating_parser
    use create_positional_test, only: &
            create_positional_creating_positional_argument => test_creating_positional_argument
    use empty_parser_test, only: &
            empty_parser_empty_parser => test_empty_parser
    use Vegetables_m, only: TestItem_t, testThat, runTests

    implicit none

    call run()
contains
    subroutine run()
        type(TestItem_t) :: tests
        type(TestItem_t) :: individual_tests(3)

        individual_tests(1) = create_parser_creating_parser()
        individual_tests(2) = create_positional_creating_positional_argument()
        individual_tests(3) = empty_parser_empty_parser()
        tests = testThat(individual_tests)

        call runTests(tests)
    end subroutine run
end program
