module create_parser_test
    use cmdff, only: &
            command_t, &
            flag_t, &
            option_t, &
            parser_t, &
            positional_t, &
            create_new_parser, &
            positional_string_argument
    use erloff, only: ErrorList_t
    use Vegetables_m, only: Result_t, TestItem_t, assertThat, describe, it

    implicit none
    private

    public :: test_creating_parser
contains
    function test_creating_parser() result(tests)
        type(TestItem_t) :: tests

        type(TestItem_t) :: individual_tests(3)

        individual_tests(1) = it( &
                "can be created with no arguments", check_new_empty_parser)
        individual_tests(2) = it( &
                "can be created with a description", check_new_described_parser)
        individual_tests(3) = it( &
                "can be created with a positional argument", &
                check_parser_with_positional_argument)
        tests = describe("a new parser", individual_tests)
    end function

    function check_new_empty_parser() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(parser_t) :: parser

        call create_new_parser( &
                program_name = "example", &
                flags = [flag_t ::], &
                options = [option_t ::], &
                arguments = [positional_t ::], &
                commands = [command_t ::], &
                errors = errors, &
                parser = parser)

        result_ = assertThat(.not.errors%hasAny(), errors%toString())
    end function

    function check_new_described_parser() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(parser_t) :: parser

        call create_new_parser( &
                program_name = "example", &
                description = "An example command line", &
                flags = [flag_t ::], &
                options = [option_t ::], &
                arguments = [positional_t ::], &
                commands = [command_t ::], &
                errors = errors, &
                parser = parser)

        result_ = assertThat(.not.errors%hasAny(), errors%toString())
    end function

    function check_parser_with_positional_argument() result(result_)
        type(Result_t) :: result_

        type(ErrorList_t) :: errors
        type(parser_t) :: parser

        call create_new_parser( &
                program_name = "example", &
                description = "An example command line", &
                flags = [flag_t ::], &
                options = [option_t ::], &
                arguments = [positional_string_argument( &
                        name = "arg1", &
                        description = "this is a string argument", &
                        required = .false.)], &
                commands = [command_t ::], &
                errors = errors, &
                parser = parser)

        result_ = assertThat(.not.errors%hasAny(), errors%toString())
    end function
end module
