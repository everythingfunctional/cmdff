module empty_parser_test
    use cmdff, only: &
            command_t, &
            flag_t, &
            option_t, &
            parsed_arguments_t, &
            parser_t, &
            positional_t, &
            new_parser
    use erloff, only: ErrorList_t
    use iso_varying_string, only: varying_string, var_str
    use Vegetables_m, only: &
            Result_t, TestItem_t, assertIncludes, assertThat, given, then_, when

    implicit none
    private

    public :: test_empty_parser
contains
    function test_empty_parser() result(tests)
        type(TestItem_t) :: tests

        type(TestItem_t) :: whens(1)
        type(TestItem_t) :: empty_commands(2)

        empty_commands(1) = then_( &
                "there are no errors", check_empty_parser_with_empty_command)
        empty_commands(2) = then_( &
                "the help message includes the program name and command", &
                check_empty_parser_help_message)
        whens(1) = when("it parses an empty command", empty_commands)
        tests = given("an empty parser", whens)
    end function

    function check_empty_parser_with_empty_command() result(result_)
        type(Result_t) :: result_

        type(parsed_arguments_t) :: arguments
        type(ErrorList_t) :: errors
        type(varying_string) :: help_message
        type(parser_t) :: parser

        parser = new_parser( &
                program_name = "example", &
                description = "An example command line", &
                flags = [flag_t ::], &
                options = [option_t ::], &
                arguments = [positional_t ::], &
                commands = [command_t ::])

        call parser%parse_command_line( &
                input_arguments = [var_str("the_command")], &
                help_message = help_message, &
                errors = errors, &
                parsed_arguments = arguments)

        result_ = assertThat(.not.errors%hasAny(), errors%toString())
    end function

    function check_empty_parser_help_message() result(result_)
        type(Result_t) :: result_

        type(parsed_arguments_t) :: arguments
        type(ErrorList_t) :: errors
        type(varying_string) :: help_message
        type(parser_t) :: parser

        parser = new_parser( &
                program_name = "example", &
                description = "An example command line", &
                flags = [flag_t ::], &
                options = [option_t ::], &
                arguments = [positional_t ::], &
                commands = [command_t ::])

        call parser%parse_command_line( &
                input_arguments = [var_str("the_command")], &
                help_message = help_message, &
                errors = errors, &
                parsed_arguments = arguments)

        result_ = &
                assertIncludes("example", help_message) &
                .and.assertIncludes("the_command", help_message)
    end function
end module
