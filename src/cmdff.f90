module cmdff
    use erloff, only: ErrorList_t
    use iso_varying_string, only: varying_string, assignment(=), operator(//), char
    use strff, only: NEWLINE

    implicit none
    private

    type, public :: parser_t
        private
        type(varying_string) :: program_name
    contains
        private
        procedure, public :: parse_command_line
    end type

    type, public :: flag_t
    end type

    type, public :: option_t
    end type

    type, public :: positional_t
    end type

    type, public :: command_t
    end type

    type, public :: parsed_arguments_t
    end type

    public :: &
            create_new_parser, &
            create_new_positional_string_argument, &
            new_parser, &
            positional_string_argument
contains
    pure subroutine create_new_parser( &
            program_name, &
            description, &
            flags, &
            options, &
            arguments, &
            commands, &
            errors, &
            parser)
        character(len=*), intent(in) :: program_name
        character(len=*), intent(in), optional :: description
        type(flag_t), intent(in) :: flags(:)
        type(option_t), intent(in) :: options(:)
        type(positional_t), intent(in) :: arguments(:)
        type(command_t), intent(in) :: commands(:)
        type(ErrorList_t), intent(out) :: errors
        type(parser_t), intent(out) :: parser

        parser%program_name = program_name
    end subroutine

    pure subroutine create_new_positional_string_argument( &
            name, &
            description, &
            required, &
            errors, &
            argument)
        character(len=*), intent(in) :: name
        character(len=*), intent(in), optional :: description
        logical, intent(in) :: required
        type(ErrorList_t), intent(out) :: errors
        type(positional_t), intent(out) :: argument
    end subroutine

    pure function new_parser( &
            program_name, &
            description, &
            flags, &
            options, &
            arguments, &
            commands) &
            result(parser)
        character(len=*), intent(in) :: program_name
        character(len=*), intent(in), optional :: description
        type(flag_t), intent(in) :: flags(:)
        type(option_t), intent(in) :: options(:)
        type(positional_t), intent(in) :: arguments(:)
        type(command_t), intent(in) :: commands(:)
        type(parser_t) :: parser

        type(ErrorList_t) :: errors

        call create_new_parser( &
                program_name, &
                description, &
                flags, &
                options, &
                arguments, &
                commands, &
                errors, &
                parser)

        if (errors%hasAny()) error stop char(errors%toString())
    end function

    pure subroutine parse_command_line( &
            self, &
            input_arguments, &
            help_message, &
            errors, &
            parsed_arguments)
        class(parser_t), intent(in) :: self
        type(varying_string), intent(in) :: input_arguments(:)
        type(varying_string), intent(out) :: help_message
        type(ErrorList_t), intent(out) :: errors
        type(parsed_arguments_t), intent(out) :: parsed_arguments

        help_message = &
                self%program_name // NEWLINE &
                // NEWLINE &
                // "Usage:" // NEWLINE &
                // "    " // input_arguments(1)
    end subroutine

    pure function positional_string_argument( &
            name, description, required) result(argument)
        character(len=*), intent(in) :: name
        character(len=*), intent(in), optional :: description
        logical, intent(in) :: required
        type(positional_t) :: argument

        type(ErrorList_t) :: errors

        call create_new_positional_string_argument( &
                name, description, required, errors, argument)

        if (errors%hasAny()) error stop char(errors%toString())
    end function
end module cmdff
